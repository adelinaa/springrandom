import beans.RandomNumber;
import config.AppConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AppConfig.class);
        context.refresh();
        RandomNumber number1 = (RandomNumber)context.getBean("randomNumber");
        System.out.println("First instance: " + number1.getValue());
        RandomNumber number2 = (RandomNumber)context.getBean("randomNumber");
        System.out.println("Second instance: " + number2.getValue());
    }
}
